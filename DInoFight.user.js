// ==UserScript==
// @name         dinorpg fight saver
// @namespace    http://fight.dinorpg.mato.run
// @version      0.1
// @description  Save dinorpg data fights to GM storage
// @author       Matoran, Edited by Biosha
// @match        http://www.dinorpg.com/dino/*/act/fight?sk=*
// @icon         https://www.google.com/s2/favicons?domain=dinorpg.com
// @grant GM_setValue
// @grant GM_getValue
// ==/UserScript==
function decodeURL(str) {
    return decodeURIComponent(str.split("+").join(" "));
}

class haxeUnserializer {
    constructor(str) {
        this.buffer= str;
        this.length= str.length;
        this.cache= [];
        this.scache= []; //cache pour les strings
        this.pos= 0;
        this.unserialized= this.unserialize();
    }
    unserialize() {
        let a= this.buffer[this.pos++],
            b= {
                a: 'readArray',
                d: 'readFloat',
                f: 'readFalse',
                i: 'readDigits',
                j: 'readEnum',
                k: 'readNaN',
                l: 'readList',
                m: 'readNegativeInfinity',
                n: 'readNull',
                o: 'readObject',
                p: 'readPositiveInfinity',
                r: 'readCache',
                t: 'readTrue',
                u: 'readMultipleNull',
                v: 'readDate',
                x: 'readError',
                y: 'readString',
                z: 'readZero',
                R: 'readStringCache'
            };
        if(b.hasOwnProperty(a)) {
            return this[b[a]]();
        } else {
            throw `Invalid char "${this.buffer[this.pos-1]}" (${this.buffer.charCodeAt(this.pos-1)}) at position ${this.pos-1}`;
        }
    }
    readArray() {
        let a= [];
        while(true) {
            let b= this.buffer[this.pos];
            if(b==="h") {
                this.pos++;
                break;
            } else if(b==="u") {
                a= a.concat(this.unserialize());
            } else {
                a.push(this.unserialize());
            }
        }
        this.cache.push(a);
        return a;
    }
    readFloat() {
        let a= this.pos;
        while(true) {
            //voir si je peut obtimiser la condition
            if(["+", ",", "-", ".", "/", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "e", "E"].indexOf(this.buffer[this.pos])<0) break;
            this.pos++;
        }
        return parseFloat(this.buffer.slice(a, this.pos));
    }
    readFalse() {
        return false;
    }
    readDigits() {
        let a= 0,
            b= (this.buffer[this.pos]==='-')?(this.pos++,true):false;
        while(true) {
            let c= this.buffer[this.pos];
            //voir si je peut obtimiser la condition
            if(['0','1','2','3','4','5','6','7','8','9'].indexOf(c)<0) break; else {
                a= (a*10)+parseInt(c);
                this.pos++;
            }
        }
        return (b)?(a*-1):a;
    }
    readEnum() {
        let a= this.unserialize(),
            b= (this.pos++, this.readDigits()),
            c= (this.pos++, this.readDigits()),
            d= [];
        while (0<c--) {
            d.push(this.unserialize());
        }
        this.cache.push(`${a}.${b}(${d.join(', ')})`);
        return `${a}.${b}(${d.join(', ')})`;
    }
    readNaN() {
        return Math.NaN;
    }
    readList() {
        let a= [];
        while(true) {
            let b= this.buffer[this.pos];
            if(b==="h") {
                this.pos++;
                break;
            } else {
                a.push(this.unserialize());
            }
        }
        this.cache.push(a);
        return a;
    }
    readNegativeInfinity() {
        return Math.NEGATIVE_INFINITY;
    }
    readNull() {
        return null;
    }
    readObject() {
        let a= {};
        while(true){
            if(this.pos>=this.length) throw "Invalid object"; else if(this.buffer[this.pos]==="g") break; else {
                let b= this.unserialize();
                if(["number","string"].indexOf(typeof b)<0) throw "Invalid object key"; else {
                    let c= this.unserialize();
                    a[b]= c;
                }
            }
        } this.pos++;
        this.cache.push(a);
        return a;
    }
    readPositiveInfinity() {
        return Math.POSITIVE_INFINITY;
    }
    readCache() {
        let a= this.readDigits();
        if(a<0||a>this.cache.length) throw "Invalid reference";
        return this.cache[a];
    }
    readTrue() {
        return true;
    }
    readMultipleNull() {
        let a= [],
            b= this.readDigits();
        for(let c=0;c<b;c++) {
            a.push(null);
        }
        return a;
    }
    readDate() {
        let a= this.pos;
        this.pos+= 19;
        return new Date(this.buffer.slice(a, this.pos));
    }
    readError() {
        throw this.unserialize();
    }
    readString() {
        let a= this.readDigits();
        if(this.buffer[this.pos++]!==":"||(this.length-this.pos)<a) throw "Invalid string length";
        else {
            let b= decodeURL(this.buffer.slice(this.pos, (this.pos+=a)));
            this.scache.push(b);
            return b;
        }
    }
    readZero() {
        return 0;
    }
    readStringCache() {
        let a= this.readDigits();
        if(a<0||a>this.scache.length) throw "Invalid string reference";
        return this.scache[a];
    }
}

(function() {
    'use strict';
    let fights = GM_getValue('fights', []);
    let embed = document.querySelector('#fight');
    let flashvars = embed.getAttribute('flashvars');
    let decodedFlashvars = new URLSearchParams(flashvars);
    let data = {};
    for(let d of decodedFlashvars) {
        data[d[0]] = d[1];
    };
    let unserialize = new haxeUnserializer(data.data)
    let finalData = unserialize.unserialized;
    fights.push(finalData);
    console.log(finalData);

    GM_setValue('fights', embed);
    console.log('total fights saved', fights.length);
})();
